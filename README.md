## Author: Amy Reichhold, areichh2@uoregon.edu ##

# Controle Calculation

According to the below website, the largest controle distance on the table is 1000 and the smallest window is 0-200. While the controle distance is greater than 200, check and see how big the controle distance is starting at 1000. If it's greater than 1000 divide the controle distance exceeding 1000 by the minimum or maximum speed for 1000(13.333 or 26, respectively), and add it to a running total. Subtract the controle distance that is exceeding 1000 from the original controle distance. Repeat this for the smaller windows until the remaining controle distance is less than 200. The resulting running total is the controle opening or closing time.

If the controle distance is one of several predetermined brevet distances, then the official rules state the specific closing time which overrides the above running total.i

# Database and Display
Any controle times entered and calculated will be stored in the database when clicking the submit button. And similarly any controle times in the database can be viewed by clicking the display button.

# Test Cases
Clicking a submit button without entering any controle times should give an error message asking for a controle time.

Clicking the display button without submitting any controle times should display a new page saying "nothing submitted yet."

Clicked the submit button after entering controle times should submit/update the controle times to the database and redirect you to the index page ready for new input.

Clicking the display button after submitting controle times should display the last submitted controle times in the database(if any).
